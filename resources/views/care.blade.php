<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Care Consumer</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://kit.fontawesome.com/bf4c629bdb.js" crossorigin="anonymous"></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            h2 {
                font-size: 2.2rem;
            }
            .rounded-btn {
                border-radius: 50px;
                max-width: 100%;
                min-width: 200px;
            }
            .cookie-notice {
                background: #f4f4f4;
            }
            .cookie-notice .btn {
                border-radius: 50px;
                width: 100px;
                background-color: #ccc;
                color: #fff;
            }
            .header {
                height: calc(100vh - 60px);
                background: url({{ url('/images/Homepage-banner_01.png') }}) no-repeat center / cover;
            }
            .header-strap {
                font-size: 1.3rem;
            }
            .image-tile {
                position: relative;
            }
            .image-overlay {
                position: absolute;
                top: 50%;
                left: 50%;
                width: 100%;
                transform: translate(-50%, -50%);
                -webkit-transform: translate(-50%, -50%);
            }
            .image-overlay-line {
                display: block;
                padding: 10px;
            }
            .image-overlay-line .text {
                padding: 10px;
                background: #fff;
            }
            .image-overlay-line:not(:first-of-type) {
                margin-top: -1px;
            }
            .image-tile-link i {
                width: 30px;
                padding: 5px;
                margin-right: 5px;
                border: 1px solid;
                border-radius: 50%;
            }
            .footer {
                background: #f4f4f4;
            }
            .footer-links {
                padding: 100px 0;
            }
        </style>
    </head>
    <body>
        <div class="cookie-notice p-3">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <p class="d-inline-block mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing <u>elit.</u></p>
                        <button type="button" class="d-inline-block ml-3 btn btn-sm">GOT IT</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="header py-5 px-3 text-white">
            <div class="container-fluid h-100">
                <div class="row align-items-center">
                    <div class="col-4">
                        <img src="{{ url('/images/logo.png') }}" class="mw-100 mr-3" width="100px" alt="" title=""/>
                        <i class="fab fa-facebook-f mr-2"></i>
                        <i class="fab fa-instagram mr-2"></i>
                        <i class="fab fa-twitter mr-2"></i>
                        <i class="fas fa-search mr-2"></i>
                    </div>
                    <div class="col-4 text-center">
                        <p class="mb-0 font-weight-bold header-strap">Care for all the family fron the No1 brand in UK pharmacy*</p>
                    </div>
                    <div class="col-4 text-right">
                        <i class="fas fa-bars"></i>
                    </div>
                </div>
                <div class="row h-100 align-items-center">
                    <div class="col-12 text-center">
                        <h1>Trusted by millions of families<br> across the UK for over 28 years</h1>
                        <p class="my-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit<br/> Aperiam delectus eos mollitia natus quidem quisquam saepe, vitae.<br/> Consectetur dignissimos.</p>
                        <button type="button" class="btn btn-lg bg-white text-primary rounded-btn">Find out more</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row py-5">
                <div class="col-6">
                    <h2 class="text-primary">Over 80 tried and trusted remedies for minor ailments</h2>
                </div>
                <div class="col-6">
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad assumenda excepturi fuga harum illo libero, magni quas. Ab accusamus consequuntur cupiditate deserunt doloremque ea fugit nulla perferendis possimus sit. Nulla.</p>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, debitis distinctio dolorem doloremque doloribus eius facilis.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-6 text-center">
                    <div class="image-tile">
                        <img src="{{ url('/images/Homepage_03.png')}}" class="w-100" alt="" title=""/>
                        <div class="image-overlay">
                            <h3>
                                <span class="image-overlay-line">
                                    <span class="text">Summer essentials survival</span>
                                </span>
                                <span class="image-overlay-line">
                                    <span class="text">guide for your holidays!</span>
                                </span>
                            </h3>
                            <p class="mt-3 font-weight-bold">Explore Guide</p>
                        </div>
                    </div>
                    <div class="my-4">
                        <div class="d-inline-block image-tile-link text-primary font-weight-bold mx-3">
                            <i class="fas fa-chevron-right"></i> Lorem ipsum dolor
                        </div>
                        <div class="d-inline-block image-tile-link text-primary font-weight-bold mx-3">
                            <i class="fas fa-chevron-right"></i> Lorem ipsum dolor
                        </div>
                    </div>
                    <p class="mb-0 text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, cupiditate distinctio ea fugit hic inventore nam numquam quidem reprehenderit tempora ullam unde! Ad aut autem consectetur doloremque est quo tempore!</p>
                    <button type="button" class="mt-4 btn btn-primary btn-lg rounded-btn">Read more</button>
                </div>
                <div class="col-12 col-lg-6 text-center">
                    <div class="image-tile">
                        <img src="{{ url('/images/Homepage_03-02.png') }}" class="w-100" alt="" title=""/>
                        <div class="image-overlay">
                            <h3>
                                <span class="image-overlay-line">
                                    <span class="text">Hero SKU Feature</span>
                                </span>
                                <span class="image-overlay-line">
                                    <span class="text">Product Section</span>
                                </span>
                            </h3>
                            <p class="mt-3 font-weight-bold">Find out more</p>
                        </div>
                    </div>
                    <div class="my-4">
                        <div class="d-inline-block image-tile-link text-primary font-weight-bold mx-3">
                            <i class="fas fa-chevron-right"></i> Lorem ipsum dolor
                        </div>
                        <div class="d-inline-block image-tile-link text-primary font-weight-bold mx-3">
                            <i class="fas fa-chevron-right"></i> Lorem ipsum dolor
                        </div>
                    </div>
                    <p class="mb-0 text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, cupiditate distinctio ea fugit hic inventore nam numquam quidem reprehenderit tempora ullam unde! Ad aut autem consectetur doloremque est quo tempore!</p>
                    <button type="button" class="mt-4 btn btn-primary btn-lg rounded-btn">Read more</button>
                </div>
            </div>
        </div>
        <div class="mt-5 footer text-secondary">
            <div class="container">
                <div class="row footer-links">
                    <div class="col-12 col-lg-3">
                        <img src="{{ url('/images/logo.png') }}" class="mw-100 mb-3" width="150px" alt="" title=""/>
                        <p class="font-weight-bold">Lorem ipsum dolor sit amet</p>
                        <p>Lorem ipsum dolor sit amet<br> Lorem ipsum dolor sit amet</p>
                        <p>Lorem ipsum dolor sit amet<br> Lorem ipsum dolor sit amet</p>
                    </div>
                    <div class="col-12 col-lg-3">
                        <h5 class="font-weight-bold">Our ranges</h5>
                        <div class="row mt-3">
                            <div class="col-6">
                                <p>Lorem ipsum</p>
                                <p>Lorem ipsum</p>
                                <p>Lorem ipsum</p>
                                <p>Lorem ipsum</p>
                            </div>
                            <div class="col-6">
                                <p>Lorem ipsum</p>
                                <p>Lorem ipsum</p>
                                <p>Lorem ipsum</p>
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <h5 class="font-weight-bold">A little care from us</h5>
                        <div class="mt-3">
                            <p>Lorem ipsum dolor</p>
                            <p>Lorem ipsum dolor</p>
                            <p>Lorem ipsum dolor</p>
                            <p>Lorem ipsum dolor</p>
                            <p>Lorem ipsum dolor</p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <h5 class="font-weight-bold">Stay in touch</h5>
                        <div class="mt-3">
                            <i class="fab fa-facebook-f mr-2"></i>
                            <i class="fab fa-instagram mr-2"></i>
                            <i class="fab fa-twitter mr-2"></i>
                        </div>
                        <div class="mt-3">
                            <p>Monday - Friday 8am - 5pm (GMT)</p>
                            <p>Telephone 01234 567890</p>
                            <p>Email address care@address.com</p>
                        </div>
                    </div>
                </div>
                <div class="row pb-5">
                    <div class="col-12 col-lg-6">
                        <p class="mb-0">&copy; Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                    </div>
                    <div class="col-12 col-lg-6 text-left text-lg-right">
                        <p class="mb-0">Link | Link | Link | Link | Link | Link | <a href="#" class="font-weight-bold">Link</a></p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
