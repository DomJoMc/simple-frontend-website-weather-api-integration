<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>OpenWeatherAPI</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <style>
            #weather-table {
                min-width: 300px;
            }
        </style>
    </head>
    <body>
        <header class="bg-primary py-3">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-3">
                        <h1 class="mb-0 text-uppercase text-white font-weight-bold">Weather</h1>
                    </div>
                    <div class="col-9">
                        <input type="text" id="search" class="form-control" placeholder="Enter a town, city or UK postcode"/>
                    </div>
                </div>
            </div>
        </header>
        <main class="py-3">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div id="weather-data" class="d-none">
                            <h2>Weather in <span id="location-name">Burnley</span></h2>
                            <h3><span id="location-temp"></span> °C</h3>
                            <div id="weather-description">Scattered clouds</div>
                            <table id="weather-table" class="table-striped table-bordered">
                                <tr>
                                    <td>Wind</td>
                                    <td id="wind"></td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td id="description"></td>
                                </tr>
                                <tr>
                                    <td>Pressure</td>
                                    <td id="pressure"></td>
                                </tr>
                                <tr>
                                    <td>Humidity</td>
                                    <td id="humidity"></td>
                                </tr>
                            </table>
                        </div>
                        <div id="weather-error" class="d-none">
                            <div class="alert alert-danger">unfortunately we couldn't retrieve weather information for that location, please try again or use a different location</div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer>
            <!-- Scripts -->
            <script src="{{ asset('js/app.js') }}" defer></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
            <script>
                $(function () {

                    $.ajaxSetup({
                        headers: {
                            // send the csrf token on all ajax requests
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    // get the page elements
                    const $searchInput = $('#search');
                    const $weatherData = $('#weather-data');
                    const $weatherError = $('#weather-error');

                    // detect keypress on the element
                    $searchInput.keypress((e) => {
                        const value = e.target.value;
                        const key = (e.keyCode ? e.keyCode : e.which);
                        if (key === 13) {
                            // if the key is 'ENTER' send an ajax request
                            $.ajax({
                                url: '{{ route('weather.get') }}',
                                method: 'POST',
                                data: {
                                    location: value
                                },
                                success: (data) => {
                                    if (data.success && data.data.cod !== '404') {
                                        showWeatherData(data.data);
                                    } else {
                                        showWeatherError();
                                    }
                                }
                            });
                        }
                    });

                    // show the weather data element
                    function showWeatherData(data) {
                        $('#location-name').text(data.name);
                        $('#location-temp').text(data.main.temp);
                        $('#wind').text(
                            data.wind.speed + ' m/s ' + data.wind.deg + '°'
                        );
                        $('#description').text(data.weather[0].description);
                        $('#pressure').text(data.main.pressure + ' hpa');
                        $('#humidity').text(data.main.humidity + '%');

                        $weatherData.removeClass('d-none');
                        $weatherError.addClass('d-none');
                    }

                    // show the weather error element
                    function showWeatherError() {
                        $weatherData.addClass('d-none');
                        $weatherError.removeClass('d-none');
                    }

                });
            </script>
        </footer>
    </body>
</html>
