<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/** open weather api */
Route::get('weather', 'OpenWeatherApiController@index')->name('weather.index');
Route::post('weather', 'OpenWeatherApiController@getWeather')->name('weather.get');

/** care consumer */
Route::get('care', function () {
    return view('care');
});
