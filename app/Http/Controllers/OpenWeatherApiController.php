<?php


namespace App\Http\Controllers;


use App\Exceptions\MissingOpenWeatherApiEndpoint;
use App\Exceptions\MissingOpenWeatherApiKey;
use Illuminate\Http\Request;
use App\Helpers\WebRequestHelper;

class OpenWeatherApiController extends Controller
{

    protected $apiEndpoint;
    protected $apiKey;

    public function __construct()
    {
        $this->apiEndpoint = env('OPEN_WEATHER_API_ENDPOINT');
        $this->apiKey = env('OPEN_WEATHER_API_KEY');
    }

    /**
     * returns the index template
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('weather');
    }

    /**
     * returns the weather data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws MissingOpenWeatherApiEndpoint
     * @throws MissingOpenWeatherApiKey
     */
    public function getWeather(Request $request)
    {
        if (!$this->apiEndpoint) {
            // if the api endpoint doesn't exist, throw an exception
            throw new MissingOpenWeatherApiEndpoint();
        }

        if (!$this->apiKey) {
            // if the api key doesn't exist, throw an exception
            throw new MissingOpenWeatherApiKey();
        }

        // get the location
        $location = $request->get('location');

        // use the WebRequestHelper class to send a cURL request to the api
        $response = WebRequestHelper::get($this->apiEndpoint . 'weather?q=' . $location . '&units=metric&appid=' . $this->apiKey);

        // return the response
        return response()->json(['success' => true, 'data' => $response]);
    }

}
