<?php


namespace App\Exceptions;

use Exception;

class MissingOpenWeatherApiKey extends Exception
{

    protected $code = 500;
    protected $message = '';

    public function __construct()
    {
         $this->message = 'missing or invalid api key';
         parent::__construct();
    }

    public function render($request)
    {
        return response()->json([
            'success' => false,
            'message' => $this->message
        ], $this->code);
    }

}
