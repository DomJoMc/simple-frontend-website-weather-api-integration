<?php

namespace App\Helpers;

class WebRequestHelper
{

    /**
     * sends a get request to the specified url
     *
     * @param string $url
     * @param array $headers
     * @return mixed|string
     */
    public static function get(string $url, array $headers = [])
    {
        return self::performRequest('GET', $url, $headers);
    }

    /**
     * sends a post request to the specified url
     *
     * @param string $url
     * @param array $data
     * @param array $headers
     * @return mixed|string
     */
    public static function post(string $url, array $data, array $headers = [])
    {
        return self::performRequest('POST', $url, $headers, $data);
    }

    /**
     * sends a curl request and returns the response
     *
     * @param string $type
     * @param string $url
     * @param array $headers
     * @param array $postData
     * @return mixed|string
     */
    private static function performRequest(string $type, string $url, array $headers = [], array $postData = [])
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_HTTPHEADER => $headers
        ));

        $response = curl_exec($curl);

        $error = null;
        if (curl_errno($curl)) {
            $error = curl_error($curl);
        }
        curl_close($curl);

        return $error ?? json_decode($response);
    }

}
